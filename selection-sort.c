#include <stdio.h>

// Equivalent to having two arrays, one sorted and the other not sorted.
// To sort the array, move the highest/lowest element from the unsorted array...
// ...place it at the right position in the sorted array.

void selectionSort(int arr[], int n) {
    int i, j, min_idx, temp;
    

    // One by one move the boundary of the unsorted subarray
    for (i = 0; i < n-1; i++) {
        // Find the minimum element in the unsorted array
        min_idx = i;
        for (j = i+1; j < n; j++){
          if (arr[j] < arr[min_idx]){
            min_idx = j;
          }
        }
        
        // Swap the found minimum element with the first element
        temp = arr[min_idx];
        arr[min_idx] = arr[i];
        arr[i] = temp;
    }
}

void printArray(int arr[], int size) {
    printf("[");
    for (int i = 0; i < size; i++) {
        printf("%d", arr[i]);
        if (i < size - 1) {
            printf(", ");
        }
    }
    printf("]\n");
}

int main() {
    int arr[] = {64, 25, 12, 22, 11};
    // Call Sort
    selectionSort(arr, 5);

    // Print results
    printf("Sorted array: \n ---> ");
    printArray(arr, 5);

    return 0;
}
#include <stdio.h>

// Builds the sorted array one at a time.

// Unsorted == [12, 11, 13, 5, 6]
// [11, 12, 13, 5, 6]
// [11, 12, 13, 5, 6]
// [5, 11, 12, 13, 6]
// [5, 6, 11, 12, 13]

void insertionSort(int arr[], int n) {
    int i, key, j;
    for (i = 1; i < n; i++) {
        key = arr[i];
        j = i - 1;

        // Move elements of arr[0..i-1], that are greater than key, to one position ahead...
        // ...of their current position
        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
}

void printArray(int arr[], int size) {
    printf("[");
    for (int i = 0; i < size; i++) {
        printf("%d", arr[i]);
        if (i < size - 1) {
            printf(", ");
        }
    }
    printf("]\n");
}

int main() {
    int arr[] = {12, 11, 13, 5, 6};

    // Call insertion sort
    insertionSort(arr, 5);

    // Print sorted array
    printArray(arr, 5);
    return 0;
}
#include <stdio.h>

// Compare any two adjust elements, sort them until the entire list is sorted.
void bubbleSort(int arr[], int n)
{
    int i, j, temp;
    for (i = 0; i < n - 1; i++)
    {
        // Last i elements are already in place
        for (j = 0; j < n - i - 1; j++){
            if (arr[j] > arr[j + 1])
            {
                temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}

void printArray(int arr[], int size)
{
    printf("[");
    for (int i = 0; i < size; i++)
    {
        printf("%d", arr[i]);
        if (i < size - 1)
        {
            printf(", ");
        }
    }
    printf("]\n");
}

int main()
{
    int arr[] = {64, 34, 25, 12, 22, 11, 90};

    // Call bubble sort
    bubbleSort(arr, 7);

    //Print sorted array
    printf("Sorted array: \n --->");
    printArray(arr, 7);
    return 0;
}